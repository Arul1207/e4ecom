import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }
  slides = [
    {image: '../assets/images/hero/hero-1.jpg',
    slogan: 'Delivering Solutions That Steer Business.'}, 
    {image: '../assets/images/hero/hero-2.jpg',
    slogan: 'We help to Build your Business.'}];  
  
  SlideOptions = { items: 1, dots: false, nav: true, loop: true, autoplay: true };  
  // CarouselOptions = { items: 3, dots: true, nav: true };  
  ngOnInit(): void {
  }

}
