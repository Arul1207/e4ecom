import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { OwlModule } from 'ngx-owl-carousel';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    OwlModule
  ],
  exports: [
    BrowserAnimationsModule,
    MaterialModule,
    OwlModule
  ]
})
export class SharedModule { }
